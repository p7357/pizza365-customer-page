import { Button, Col, Container, Row } from "reactstrap"
import { TiArrowUpThick, TiSocialFacebook, TiSocialInstagram, TiSocialTwitter, TiSocialLinkedin} from "react-icons/ti"
function FooterComponent() {
  return (
    <Container>
      <Row>
        <Col xs={12} className="text-center">
          <h4 className="mb-3">Footer</h4>
          <Button color="dark" href="#" className="mb-3" style={{fontWeight:"bold"}}><TiArrowUpThick style={{fontSize:"20px"}}/> To The Top  </Button>          
          <div className="mb-3">
            <TiSocialFacebook style={{fontSize:"30px"}} className="mx-2"/>
            <TiSocialInstagram style={{fontSize:"30px"}} className="mx-2"/>
            <TiSocialTwitter style={{fontSize:"30px"}} className="mx-2"/>
            <TiSocialLinkedin style={{fontSize:"30px"}} className="mx-2"/>
          </div>
        </Col>
      </Row>
    </Container>
  )
}

export default FooterComponent