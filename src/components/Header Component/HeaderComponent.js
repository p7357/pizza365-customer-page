import { Col, Collapse, Container, Nav, Navbar, NavItem, NavLink, Row } from "reactstrap"

function HeaderComponent() {
  return (
    <Container>
      <Row>
        <Col xs={12}>
          <Navbar color="dark" dark expand="sm" fixed="top">
            <Collapse navbar>
              <Nav navbar style={{paddingLeft:"140px"}}>
                <NavItem>
                  <NavLink className="text-warning mx-5" href="#">
                    Trang chủ
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className="text-warning mx-5" href="#about">
                    About Us
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className="text-warning mx-5" href="#size">
                    Menu
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className="text-warning mx-5" href="#type">
                    Loại Pizza
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className="text-warning mx-5" href="#drink">
                    Nước uống
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className="text-warning mx-5" href="#form">
                    Đặt hàng
                  </NavLink>
                </NavItem>
              </Nav>
            </Collapse>
          </Navbar>
        </Col>
      </Row>
    </Container>
  )
}

export default HeaderComponent