import { Col, FormGroup, Input, Label, Row } from "reactstrap"
import axios from "axios";

function FormComponent({ getDataHandler, userInput, inputValidate, inputError, getDiscount }) {
  const axiosAPI = async (paramUrl, paramOptions = {}) => {
    const response = await axios(paramUrl, paramOptions);
    const responseData = response.data;
    return responseData;
}

  
  const getName = (event) => {
    getDataHandler({...userInput , hoTen : event.target.value})
    if(event.target.value === ""){
      inputValidate({...inputError, name: "Bạn chưa nhập họ tên"})
    } else {
      inputValidate({...inputError, name: "none"})
    }
  }

  const getPhone = (event) => {
    getDataHandler({...userInput , soDienThoai : event.target.value})
    if (event.target.value === "" || isNaN(event.target.value) || event.target.value.length !== 10) {
      inputValidate({...inputError, phone: "Số điện thoại phải là số có mười số"})
    } else {
      inputValidate({...inputError, phone: "none"})
    }
  }

  const getAddress = (event) => {
    getDataHandler({...userInput , diaChi : event.target.value})
    if(event.target.value === ""){
      inputValidate({...inputError, address: "Bạn chưa nhập địa chỉ"})
    } else {
      inputValidate({...inputError, address: "none"})
    }
  }

  const getEmail = (event) => {
    getDataHandler({...userInput , email : event.target.value})
    if(/\S+@\S+\.\S+/.test(event.target.value) === false && event.target.value !== ""){
      inputValidate({...inputError, email: "Email chưa đúng cú pháp"})
    } else {
      inputValidate({...inputError, email: "none"})
    }
  }

  const getVoucher = (event) => {
    getDataHandler({...userInput , idVourcher : event.target.value})
    if( event.target.value === "")  {
      inputValidate({...inputError, voucher: "none"})
    } else if (isNaN(event.target.value)) {
      inputValidate({...inputError, voucher: "Voucher không hợp lệ"})
    } else {
      inputValidate({...inputError, voucher: "none"})
      axiosAPI(`http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/${event.target.value}`)
      .then((data) => {
          console.log(data);
          getDiscount(data.phanTramGiamGia + "%")
      })
      .catch((error) => {
          console.log("Voucher không tồn tại");
          getDiscount("Mã giảm giá không tồn tại")
      })
    }
  }

  const getNote = (event) => {
    getDataHandler({...userInput , loiNhan : event.target.value})
  }
  return (
    <Row id="form">
      {/* <!-- Title Contact Us --> */}
      <Col xs={12} className="text-center p-4 mt-4">
        <h2><b className="p-2 border-bottom">Gửi đơn hàng</b></h2>
      </Col>

      {/* <!-- Content Contact Us --> */}
      <Col xs={12} className="p-2">
        <Row>
          <Col xs={12}>
            <FormGroup className="mb-3">
              <Label>Họ và tên</Label>
              <Input placeholder="Họ và tên" onChange={getName}/>
            </FormGroup>
            <FormGroup className="mb-3">
              <Label>Số điện thoại</Label>
              <Input placeholder="Số điện thoại" onChange={getPhone}/>
            </FormGroup>
            <FormGroup className="mb-3">
              <Label>Địa chỉ</Label>
              <Input placeholder="Địa chỉ" onChange={getAddress}/>
            </FormGroup>
            <FormGroup className="mb-3">
              <Label>Email</Label>
              <Input placeholder="Email" onChange={getEmail}/>
            </FormGroup>
            <FormGroup className="mb-3">
              <Label>Mã giảm giá</Label>
              <Input placeholder="Mã giảm giá" onChange={getVoucher}/>
            </FormGroup>
            <FormGroup className="mb-3">
              <Label>Lời nhắn</Label>
              <Input placeholder="Lời nhắn" onChange={getNote}/>
            </FormGroup>
          </Col>
        </Row>
      </Col>
    </Row>
  )
}

export default FormComponent