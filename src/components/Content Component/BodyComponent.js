import { Row } from "reactstrap"
import IntroduceComponent from "./Introduce Component/IntroduceComponent"
import SizeComponent from "./Size Component/SizeComponent"
import TypeComponent from "./Type Component/TypeComponent"
import DrinkComponent from "./Drink Component/DrinkComponent"
import FormComponent from "./Form Component/FormComponent"
import { useEffect, useState } from "react"
import SubmitButton from "./Submit Button/SubmitButton"

function BodyComponent() {
    const [comboError, setComboError] = useState("Bạn chưa chọn cỡ combo")
    const [combo, setCombo] = useState({
        kichCo: "Chưa chọn",
        duongKinh: "",
        suon: "",
        salad: "",
        soLuongNuoc: "",
        thanhTien: ""
    })
    const [type, setType] = useState({
        loaiPizza: "",
        tenPizza: "",
    })
    const [typeError, setTypeError] = useState("Bạn chưa chọn loại Pizza")

    const [drink, setDrink] = useState({
        idLoaiNuocUong: "",
        tenNuocUong: "",
    })
    const [drinkError, setDrinkError] = useState("Bạn chưa chọn nước uống")

    const [inputError, setInputError] = useState({
        name: "Bạn chưa nhập họ tên",
        phone: "Bạn chưa nhập số điện thoại",
        address: "Bạn chưa nhập địa chỉ",
        email: "none",
        voucher: "none"
    })
    const [userInput, setUserInput] = useState({
        hoTen: "",
        soDienThoai: "",
        diaChi: "",
        email: "",
        idVourcher: "",
        loiNhan: ""
    })

    const [discount, setDiscount] = useState("")

    useEffect(() => {
        console.log(comboError + " " + typeError + " " + drinkError)
        console.log(inputError)
        console.log(combo)
        console.log(type.tenPizza + " " + drink.tenNuocUong)
        console.log(userInput)
    })

    return (
        <Row style={{ paddingLeft: "40px" }}>
            <IntroduceComponent />
            <SizeComponent getSizeHandler={setCombo} sizeValidate={setComboError} />
            <TypeComponent getTypeHandler={setType} typeValidate={setTypeError} type={type}/>
            <DrinkComponent getDrinkHandler={setDrink} drinkValidate={setDrinkError} drink={drink} />
            <FormComponent getDataHandler={setUserInput} userInput={userInput} 
            inputValidate={setInputError} inputError={inputError} getDiscount={setDiscount}/>
            <SubmitButton combo={combo} type={type} drink={drink} userInput={userInput} discount={discount}
            comboError={comboError} typeError={typeError} drinkError={drinkError} inputError={inputError} />
        </Row>
    )
}

export default BodyComponent