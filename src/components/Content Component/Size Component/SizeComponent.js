import { Button, Card, CardBody, CardFooter, CardHeader, Col, ListGroup, ListGroupItem, Row } from "reactstrap"


function SizeComponent( { getSizeHandler, sizeValidate } ) {
  const menuList = [
    { kichCo: "S", duongKinh: "20", suon: "2", salad: "200", soLuongNuoc: "2", thanhTien: "150000" },
    { kichCo: "M", duongKinh: "25", suon: "3", salad: "300", soLuongNuoc: "3", thanhTien: "200000" },
    { kichCo: "L", duongKinh: "30", suon: "4", salad: "400", soLuongNuoc: "4", thanhTien: "250000" }
]

  const pickSizeS = () => {
    getSizeHandler(menuList[0])
    sizeValidate("none")
  }

  const pickSizeM = () => {
    getSizeHandler(menuList[1])
    sizeValidate("none")
  }

  const pickSizeL = () => {
    getSizeHandler(menuList[2])
    sizeValidate("none")
  }

  return (
    <Row id="size">
      {/* <!-- Title what we offer --> */}
      <Col xs={12} className="text-center p-4 mt-4">
        <h2><b className="p-1 border-bottom">Menu Combo Pizza 365</b></h2>
        <p><span className="p-2">Hãy chọn cỡ pizza phù hợp với bạn!</span></p>
      </Col>
      {/* <!-- Content what we offer --> */}
      <Col xs={12}>
        <Row>
          <Col xs={4}>
            <Card className="text-center">
              <CardHeader tag="h3" className="bg-warning text-dark">
                Small
              </CardHeader>
              <CardBody>
                <ListGroup flush>
                  <ListGroupItem><b>Đường kính:</b> 20cm</ListGroupItem>
                  <ListGroupItem><b>Sườn nướng:</b> 2</ListGroupItem>
                  <ListGroupItem><b>Salad:</b> 200g</ListGroupItem>
                  <ListGroupItem><b>Nước ngọt:</b> 2</ListGroupItem>
                  <ListGroupItem><h1><b>150.000</b></h1> VND</ListGroupItem>
                </ListGroup>                  
              </CardBody>
              <CardFooter>
                <Button color="success" onClick={pickSizeS}>Chọn</Button>
              </CardFooter>
            </Card>
          </Col>
          <Col xs={4}>
          <Card className="text-center">
              <CardHeader tag="h3" className="bg-dark text-warning">
                Medium
              </CardHeader>
              <CardBody>
                <ListGroup flush>
                  <ListGroupItem><b>Đường kính:</b> 25cm</ListGroupItem>
                  <ListGroupItem><b>Sườn nướng:</b> 3</ListGroupItem>
                  <ListGroupItem><b>Salad:</b> 300g</ListGroupItem>
                  <ListGroupItem><b>Nước ngọt:</b> 3</ListGroupItem>
                  <ListGroupItem><h1><b>200.000</b></h1> VND</ListGroupItem>
                </ListGroup>
              </CardBody>
              <CardFooter>
                <Button color="success" onClick={pickSizeM}>Chọn</Button>
              </CardFooter>
            </Card>
          </Col>
          <Col xs={4}>
          <Card className="text-center">
              <CardHeader tag="h3" className="bg-warning text-dark">
                Large
              </CardHeader>
              <CardBody>
                <ListGroup flush>
                  <ListGroupItem><b>Đường kính:</b> 30cm</ListGroupItem>
                  <ListGroupItem><b>Sườn nướng:</b> 4</ListGroupItem>
                  <ListGroupItem><b>Salad:</b> 400g</ListGroupItem>
                  <ListGroupItem><b>Nước ngọt:</b> 4</ListGroupItem>
                  <ListGroupItem><h1><b>250.000</b></h1> VND</ListGroupItem>
                </ListGroup>
              </CardBody>
              <CardFooter>
                <Button color="success" onClick={pickSizeL}>Chọn</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </Col>
    </Row>
  )
}

export default SizeComponent