import { Col, Input, Row } from "reactstrap"
import axios from "axios";
import { useEffect, useState } from "react"

function DrinkComponent( { getDrinkHandler, drinkValidate, drink }) {
    
    const [drinkList, setDrinkList] = useState([])

    const pickDrink = (event) => {
        getDrinkHandler({tenNuocUong: event.target.options[event.target.selectedIndex].text , idLoaiNuocUong: event.target.value})
        drinkValidate("none")
    }

    const axiosAPI = async (paramUrl, paramOptions = {}) => {
        const response = await axios(paramUrl, paramOptions);
        const responseData = response.data;
        return responseData;
    }

    useEffect (() => {
        axiosAPI(`http://42.115.221.44:8080/devcamp-pizza365/drinks`)
        .then((data) => {
            setDrinkList(data)
        })
        .catch((error) => {
            console.log(error);
        })
    })

    return (
        <Row id="drink">
            <Col xs={12} className="text-center p-4 mt-4">
                <h2><b className="p-2 border-bottom">Chọn nước uống</b></h2>
            </Col>
            <Col xs={12}>
                <Input type="select" id="select-drink" onChange={pickDrink}>
                    <option value="none">Chọn nước uống</option>
                    {drinkList.map((element, index) => {
                        return <option key={index}  text={element.tenNuocUong} value={element.maNuocUong}>{element.tenNuocUong}</option>
                    }
                    )}
                </Input>
            </Col>
        </Row>
    )
}

export default DrinkComponent