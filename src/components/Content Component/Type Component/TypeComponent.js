import { Button, Card, CardBody, CardImg, CardText, CardTitle, Col, Row } from "reactstrap"
import PizzaPic1 from "../../../assets/images/pizza_type_1.jpg"
import PizzaPic2 from "../../../assets/images/pizza_type_2.jpg"
import PizzaPic3 from "../../../assets/images/pizza_type_3.jpg"

function TypeComponent({ getTypeHandler, typeValidate, type }) {
  const typeList = [
    {loaiPizza: "SEAFOOD", tenPizza: "Hải sản"},
    {loaiPizza: "HAWAII", tenPizza: "Hawaii"},
    {loaiPizza: "BACON", tenPizza: "Thịt xông khói"}
  ]

  const pickSeafood = () => {
    getTypeHandler(typeList[0])
    typeValidate("none")
  }

  const pickHawaii = () => {
    getTypeHandler(typeList[1])
    typeValidate("none")
  }

  const pickBacon = () => {
    getTypeHandler(typeList[2])
    typeValidate("none")
  }

  return (
    <Row id="type">
      <Col xs={12} className="text-center p-4 mt-4" >
        <h2><b className="p-2 border-bottom">Chọn loại pizza</b></h2>
      </Col>

      <Col xs={12}>
        <Row>
          <Col xs={4}>
            <Card>
              <CardImg src={PizzaPic1} top />
              <CardBody>
                <CardTitle tag="h3">
                  Pizza Seafood
                </CardTitle>
                <CardText>
                  Tôm, mực, bạch tuộc, dứa thái lát, sốt cà chua, phô mai morazella Anchor
                </CardText>
                <Button color="success" className="w-100" onClick={pickSeafood}> Chọn </Button>
              </CardBody>
            </Card>
          </Col>
          <Col xs={4}>
            <Card>
              <CardImg src={PizzaPic2} top />
              <CardBody>
                <CardTitle tag="h3">
                  Pizza Hawaii
                </CardTitle>
                <CardText>
                  Giăm bông, dứa thái lát, sốt cà chua, phô mai morazella Anchor
                </CardText>
                <Button color="success" className="w-100" onClick={pickHawaii}> Chọn </Button>
              </CardBody>
            </Card>
          </Col>
          <Col xs={4}>
            <Card>
              <CardImg src={PizzaPic3} top />
              <CardBody>
                <CardTitle tag="h3">
                  Pizza Bacon
                </CardTitle>
                <CardText>
                  Thịt xông khói, ớt xanh, ớt đỏ, hành tây, salami xúc xích, phô mai Mozzarella Anchor
                </CardText>
                <Button color="success" className="w-100" onClick={pickBacon}> Chọn </Button>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Col>
    </Row>

  )
}

export default TypeComponent