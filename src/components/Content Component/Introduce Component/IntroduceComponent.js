import { Col, Row, UncontrolledCarousel } from "reactstrap"
import Italy1 from "../../../assets/images/mon_y_1.jpg"
import Italy2 from "../../../assets/images/mon_y_2.jpg"
import Italy3 from "../../../assets/images/mon_y_3.jpg"
import Italy4 from "../../../assets/images/mon_y_4.jpg"
import Italy5 from "../../../assets/images/mon_y_5.jpg"

function IntroduceComponent() {
  return (
    <Col xs={12}>
      <Row>
        {/* <!-- Title row home --> */}
        <Col xs={12} className="bg-warning">
          <h1 style={{ fontStyle: "italic", fontWeight: "bold" }}>PIZZA 365</h1>
        </Col>
        {/* <!-- Slide row home --> */}
        <UncontrolledCarousel
          items={[
            {
              altText: '',
              caption: '',
              key: 1,
              src: Italy1 
            },
            {
              altText: '',
              caption: '',
              key: 2,
              src: Italy2
            },
            {
              altText: '',
              caption: '',
              key: 3,
              src: Italy3
            },
            {
              altText: '',
              caption: '',
              key: 4,
              src: Italy4
            },
            {
              altText: '',
              caption: '',
              key: 5,
              src: Italy5
            }
          ]}
        ></UncontrolledCarousel>


        {/* <!-- Title what we offer --> */}
        <Col xs={12} className="text-center p-4 mt-4" id="about">
          <h2><b className="p-2 border-bottom">Vì sao nên chọn Pizza 365</b></h2>
        </Col>
        <Row>
          <Col xs={3} className="p-4 bg-dark text-warning border">
            <h3 className="p-2">Sự đa dạng</h3>
            <p className="p-2">Pizza 365 có đầy đủ các loại đang thịnh hành nhất và luôn cập nhật công thức mới</p>
          </Col>
          <Col xs={3} className="p-4 bg-warning text-dark border">
            <h3 className="p-2">Chất lượng</h3>
            <p className="p-2">Nguyên liệu tươi mới, quy trình chế biến đạt chuẩn vệ sinh an toàn thực phẩm.</p>
          </Col>
          <Col xs={3} className="p-4 bg-dark text-warning border">
            <h3 className="p-2">Hương vị</h3>
            <p className="p-2">Đảm bảo hương vị thơm ngon, độc đáo mà bạn chỉ có thể trải nghiệm tại Pizza 365</p>
          </Col>
          <Col xs={3} className="p-4 bg-warning text-dark border">
            <h3 className="p-2">Dịch vụ</h3>
            <p className="p-2">Chất lượng dịch vụ, sự hài lòng của khách hàng luôn là sự quan tâm hàng đầu của Pizza 365</p>
          </Col>
        </Row>
      </Row>
    </Col>
  )
}

export default IntroduceComponent