import { useState } from "react"
import { Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row } from "reactstrap"
import axios from "axios";

function SubmitButton({ combo, type, drink, userInput, comboError, typeError, drinkError, inputError, discount }) {
  const axiosAPI = async (paramUrl, paramOptions = {}) => {
    const response = await axios(paramUrl, paramOptions);
    const responseData = response.data;
    return responseData;
}
  const [discountMessage, setDiscountMessage] = useState("")
  const [price, setPrice] = useState("")
  const [orderId, setOrderId] = useState("")
  const [errorModal, setErrorModal] = useState(false)
  const [infoModal, setInfoModal] = useState(false)
  const [confirmModal, setConfirmModal] = useState(false)

  const closeModal = () => {
    setInfoModal(false)
    setErrorModal(false)
  }

  const closeConfirmModal = () => {
    setConfirmModal(false)
  }

  const onBtnSendClick = () => {
    if (comboError === "none" && typeError === "none" && drinkError === "none" &&
      inputError.name === "none" && inputError.phone === "none" &&
      inputError.address === "none" && inputError.email === "none" && inputError.voucher === "none") {
      setInfoModal(true)
      checkDiscount()
    } else {
      setErrorModal(true)
    }
  }
  const checkDiscount = () => {
    if(userInput.idVourcher === ""){
      setDiscountMessage("Không có mã giảm giá") 
      setPrice(combo.thanhTien)
    } else if(discount === "Mã giảm giá không tồn tại") {
      setDiscountMessage("Mã giảm giá không tồn tại") 
      setPrice(combo.thanhTien)
    } else {
      setDiscountMessage("Mã giảm giá: " + discount) 
      setPrice( parseInt(combo.thanhTien) * (100 - parseInt(discount)) )
    }
  }
  const createOrder = () => {
      const body = {
          method: 'POST',
          data: {
              kichCo: combo.kichCo,
              duongKinh: combo.duongKinh,
              suon: combo.suon,
              salad: combo.salad,
              loaiPizza: type.loaiPizza,
              idVourcher: combo.idVourcher,
              idLoaiNuocUong: drink.idLoaiNuocUong,
              soLuongNuoc: combo.soLuongNuoc,
              hoTen: userInput.hoTen,
              thanhTien: combo.thanhTien,
              email: userInput.email,
              soDienThoai: userInput.soDienThoai,
              diaChi: userInput.diaChi,
              loiNhan: userInput.loiNhan
          },
          headers: {
            'Content-type': "application/json;charset=UTF-8",
          },
      }
      axiosAPI("http://42.115.221.44:8080/devcamp-pizza365/orders", body)
          .then((data) => {
              console.log(data);
              setOrderId(data.orderId)
              setConfirmModal(true)
              closeModal()
          })
          .catch((error) => {
              console.log(error);
          })
  }
  return (
    <>
      <Col xs={12} style={{ paddingLeft: "10px", paddingRight: "25px" }}>
        <Button className="w-100" color="dark" onClick={onBtnSendClick}>Gửi đơn</Button>
      </Col>
      {/* Modal hiển thị lỗi */}
      <Modal isOpen={errorModal} toggle={closeModal}>
        <ModalHeader>
          Đã có vài lỗi xảy ra!
        </ModalHeader>
        <ModalBody className="px-4">
          <Row style={{ display: comboError === "none" ? "none" : "block" }}>
            {comboError}
          </Row>
          <Row style={{ display: typeError === "none" ? "none" : "block" }}>
            {typeError}
          </Row>
          <Row style={{ display: drinkError === "none" ? "none" : "block" }}>
            {drinkError}
          </Row>
          <Row style={{ display: inputError.name === "none" ? "none" : "block" }}>
            {inputError.name}
          </Row>
          <Row style={{ display: inputError.phone === "none" ? "none" : "block" }}>
            {inputError.phone}
          </Row>
          <Row style={{ display: inputError.address === "none" ? "none" : "block" }}>
            {inputError.address}
          </Row>
          <Row style={{ display: inputError.email === "none" ? "none" : "block" }}>
            {inputError.email}
          </Row>
          <Row style={{ display: inputError.voucher === "none" ? "none" : "block" }}>
            {inputError.voucher}
          </Row>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={closeModal}>Quay lại</Button>
        </ModalFooter>
      </Modal>

      {/* Modal hiển thị thông tin */}
      <Modal isOpen={infoModal} size="lg" toggle={closeModal}>
        <ModalHeader>
          Xác nhận đơn đặt hàng
        </ModalHeader >
        <ModalBody>
          <Row className="border-bottom mb-3">
            <h6>Thông tin khách hàng</h6>
            <Row style={{paddingLeft:"30px", paddingTop:"10px"}}>
              <Col xs={6}>
                <p>Họ tên: {userInput.hoTen} </p> 
                <p>Số điện thoại: {userInput.soDienThoai} </p>
                <p>Địa chỉ: {userInput.diaChi} </p>
              </Col>
              <Col xs={6}>
                <p>Email: {userInput.email} </p>
                <p>{discountMessage}</p>
                <p> {userInput.loiNhan === "" ? null : "Lời nhắn: " + userInput.loiNhan} </p>
              </Col>
            </Row>
          </Row>
          <Row>
            <h6>Thông tin đơn hàng</h6>
            <Row style={{paddingLeft:"30px", paddingTop:"10px"}}>
              <Col xs={6}>
                <p>Combo: {combo.kichCo} </p>
                <p>Đường kính: {combo.duongKinh} cm</p>
                <p>Sườn nướng: {combo.suon} miếng </p>
                <p>Salad: {combo.salad} gram </p>
              </Col>
              <Col xs={6}>
              <p>Loại Pizza: {type.tenPizza} </p>
              <p>Loại nước: {drink.tenNuocUong} </p>
              <p>Số lượng nước: {combo.soLuongNuoc} ly </p>
              <p>Thành tiền: {price} VND </p>
              </Col>
            </Row>
          </Row>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={closeModal}>Quay lại</Button>
          <Button id="btn-confirm-order" color="warning" onClick={createOrder} >Tạo đơn</Button>
        </ModalFooter>
      </Modal>

      {/* Modal hiển thị thông tin đơn hàng đã được tạo */}
      <Modal isOpen={confirmModal} toggle={closeConfirmModal}>
        <ModalBody className="text-center">
          Đơn hàng của bạn đã được tạo <br />
          Mã đơn hàng của bạn là: {orderId} <br />
          Cảm ơn bạn đã tin chọn Pizza 365!
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={closeConfirmModal}>Quay lại</Button>
        </ModalFooter>
      </Modal>
    </>
  )
}

export default SubmitButton