import "bootstrap/dist/css/bootstrap.min.css";
import HeaderComponent from "./components/Header Component/HeaderComponent";
import FooterComponent from "./components/Footer Component/FooterComponent";
import BodyComponent from "./components/Content Component/BodyComponent";

function App() {
  return (
    <div >
      {/* Nav Bar */}
      <HeaderComponent />
      {/* Content */}
      <div className="container" style={{ padding: "120px 0 50px 0" }}>
        <BodyComponent />
      </div>
      {/* <!-- Footer --> */}
      <FooterComponent />
    </div>
  );
}

export default App;
